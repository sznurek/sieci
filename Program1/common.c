// Łukasz Dąbek 247929
#include "common.h"

int get_micros_elapsed(const struct timespec* start) {
    struct timespec stop;

    clock_gettime(CLOCK_MONOTONIC, &stop);
    return (stop.tv_sec - start->tv_sec) * 1000000 + (stop.tv_nsec - start->tv_nsec) / 1000;
}

