// Łukasz Dąbek 247929
#ifndef __ICMP_H__
#define __ICMP_H__

#define _POSIC_C_SOURCE 199309L
#define _BSD_SOURCE

#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/time.h>

#define ICMP_HEADER_LEN 8
#define PACKETS_PER_TRY 3
#define ICMP_TIMEOUT 1000000 // in microseconds

#define HANDLE(expr) if((expr) == -1) { perror(#expr); exit(1); }

struct traceroute_ctx {
    int sock;
    struct sockaddr_in remote_addr;
    int id;
    int last_ttl;
    int last_seq;
};

struct waiting_for {
    int seqs[PACKETS_PER_TRY];
    struct timespec send_times[PACKETS_PER_TRY];
};

struct reply {
    int is_echo_reply[PACKETS_PER_TRY];
    int durations[PACKETS_PER_TRY]; // in microseconds, -1 when not arrived
    struct in_addr reply_addr[PACKETS_PER_TRY];
};

void reply_clear(struct reply* reply);
int traceroute_ctx_new(struct traceroute_ctx* ctx, const char* addr);
unsigned short in_cksum(const unsigned short *addr, register int len, unsigned short csum);
int send_echo_requests(struct traceroute_ctx* ctx, struct waiting_for* result);
int wait_for_next_hop(const struct traceroute_ctx* ctx, const struct waiting_for* waiting, struct reply* reply);

#endif

