// Łukasz Dąbek 247929
#include <stdio.h>
#include <stdlib.h>
#include <error.h>
#include <string.h>
#include "icmp.h"

static int mean_time(const struct reply* reply);
static int received_responses(const struct reply* reply);
static void print_addresses(const struct reply* reply);
static void print_response(int try, const struct reply* reply);
static int is_echo_reply(const struct reply* reply);

int main(int argc, char** argv) {
    if(argc < 2) {
        fprintf(stderr, "usage: %s [ip]\n", argv[0]);
        return 1;
    }

    struct traceroute_ctx ctx;
    struct waiting_for waiting;
    struct reply reply;

    if(traceroute_ctx_new(&ctx, argv[1]) != 0) {
        fprintf(stderr, "invalid ip address.\n");
        return 1;
    }
    reply_clear(&reply);

    while(!is_echo_reply(&reply) && ctx.last_ttl < 32) {
        if(send_echo_requests(&ctx, &waiting) != 0) {
            return 1;
        }

        if(wait_for_next_hop(&ctx, &waiting, &reply) != 0) {
            return 1;
        }

        print_response(ctx.last_ttl, &reply);
    }

    return 0;
}

static void print_response(int try, const struct reply* reply) {
    int responses = received_responses(reply);
    int mean = mean_time(reply);

    printf("%d. ", try);
    print_addresses(reply);
    if(responses > 0 && responses < PACKETS_PER_TRY) {
        printf("???\n");
    } else if(responses == PACKETS_PER_TRY) {
        printf("%.3fms\n", (float)mean/1000.0);
    } else {
        printf("\n");
    }
}

static void print_addresses(const struct reply* reply) {
    char name_buffer[32] = "*";
    int responses = received_responses(reply);

    if(responses == 0) {
        printf("* ");
    } else {
        for(int i = 0; i < PACKETS_PER_TRY; i++) {
            if(reply->durations[i] < 0) {
                continue;
            }

            int was_printed = 0;
            for(int j = 0; j < i; j++) {
                if(reply->durations[j] < 0)
                    continue;

                if(reply->reply_addr[j].s_addr == reply->reply_addr[i].s_addr) {
                    was_printed = 1;
                    break;
                }
            }

            if(!was_printed) {
                inet_ntop(AF_INET, &(reply->reply_addr[i]), name_buffer, sizeof(name_buffer));
                printf("%s ", name_buffer);
            }
        }
    }
}

static int is_echo_reply(const struct reply* reply) {
    int exists_reply = 0;
    int exists_ttl = 0;

    for(int i = 0; i < PACKETS_PER_TRY; i++) {
        if(reply->durations[i] < 0)
            continue;

        if(reply->is_echo_reply[i])
            exists_reply = 1;
        else
            exists_ttl = 1;
    }

    if(exists_reply && exists_ttl) {
        fprintf(stderr, "malformed response.\n");
        exit(1);
    }

    return exists_reply;
}

static int received_responses(const struct reply* reply) {
    int sum = 0;

    for(int i = 0; i < PACKETS_PER_TRY; i++) {
        if(reply->durations[i] >= 0)
            sum += 1;
    }

    return sum;
}

static int mean_time(const struct reply* reply) {
    int sum = 0;

    for(int i = 0; i < PACKETS_PER_TRY; i++) {
        if(reply->durations[i] < 0)
            return -1;

        sum += reply->durations[i];
    }

    return sum/3;
}

