// Łukasz Dąbek 247929
#include "sockets.h"
#include "common.h"

#include <sys/select.h>

int recvfrom_timeout(int sock, void* buf, size_t len, int flags, struct sockaddr* addr, socklen_t* addrlen, int* micros) {
    fd_set set;

    FD_ZERO(&set);
    FD_SET(sock, &set);

    struct timeval tv;
    tv.tv_sec = *micros / 1000000;
    tv.tv_usec = *micros % 1000000;

    struct timespec start;
    clock_gettime(CLOCK_MONOTONIC, &start);

    int ready;
    HANDLE(ready = select(sock + 1, &set, NULL, NULL, &tv));

    if(ready == 0) {
        return 0;
    }

    int bytes;
    HANDLE(bytes = recvfrom(sock, buf, len, flags, addr, addrlen));
    *micros -= get_micros_elapsed(&start);
    if(*micros < 0)
        *micros = 0;

    return bytes;
}

void sendto_timeout(int sock, const void* buf, size_t len, int flags, const struct sockaddr* addr, socklen_t addrlen, int micros) {
    fd_set set;

    FD_ZERO(&set);
    FD_SET(sock, &set);

    struct timeval tv;
    tv.tv_sec = micros / 1000000;
    tv.tv_usec = micros % 1000000;
    int ready;
    HANDLE(ready = select(sock + 1, NULL, &set, NULL, &tv));

    if(ready == 0) {
        fprintf(stderr, "sendto timeout\n");
        exit(1);
    }

    HANDLE(sendto(sock, buf, len, flags, addr, addrlen));
}

