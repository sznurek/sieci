// Łukasz Dąbek 247929
#ifndef __COMMON_H__
#define __COMMON_H__

#define _POSIX_C_SOURCE 199309L
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/times.h>
#include <sys/types.h>

#define HANDLE(expr) if((expr) == -1) { perror(#expr); exit(1); }

int get_micros_elapsed(const struct timespec* start);

#endif
