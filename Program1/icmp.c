// Łukasz Dąbek 247929
#define _POSIX_C_SOURCE 199309L

#include "icmp.h"
#include "common.h"
#include "sockets.h"
#include <sys/select.h>
#include <stdint.h>
#include <sys/types.h>
#include <strings.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>

static struct icmp*
get_ttl_packet(const struct traceroute_ctx* ctx, unsigned short seq, struct ip* packet) {
    char* buffer = (char*) packet;
    struct icmp* icmp_packet = (struct icmp*) (buffer + packet->ip_hl * 4);

    if(icmp_packet->icmp_type != ICMP_TIME_EXCEEDED ||
       icmp_packet->icmp_code != ICMP_EXC_TTL) {
        return NULL;
    }

    struct ip* ip_packet_orig = (struct ip*) (buffer + packet->ip_hl * 4 + ICMP_HEADER_LEN);
    if(ip_packet_orig->ip_p != IPPROTO_ICMP) {
        return NULL;
    }

    struct icmp* icmp_packet_orig = (struct icmp*) (buffer + packet->ip_hl * 4 + ICMP_HEADER_LEN + ip_packet_orig->ip_hl * 4);
    if(htons(ctx->id) != icmp_packet_orig->icmp_id || htons(seq) != icmp_packet_orig->icmp_seq) {
        return NULL;
    }

    return icmp_packet_orig;
}

static struct icmp* get_echo_reply_packet(const struct traceroute_ctx* ctx, unsigned short seq, struct ip* packet) {
    char* buffer = (char*) packet;
    struct icmp* icmp_packet = (struct icmp*) (buffer + packet->ip_hl * 4);

    if(icmp_packet->icmp_type != ICMP_ECHOREPLY ||
       icmp_packet->icmp_id != htons(ctx->id) ||
       icmp_packet->icmp_seq != htons(seq) ||
       packet->ip_src.s_addr != ctx->remote_addr.sin_addr.s_addr) {
        return NULL;
    }

    return icmp_packet;
}

void reply_clear(struct reply* reply) {
    for(int i = 0; i < PACKETS_PER_TRY; i++) {
        reply->durations[i] = -1;
        reply->is_echo_reply[i] = 0;
        bzero(&(reply->reply_addr[i]), sizeof(reply->reply_addr[i]));
    }
}

int traceroute_ctx_new(struct traceroute_ctx* ctx, const char* addr) {
    bzero(&(ctx->remote_addr), sizeof(ctx->remote_addr));
    ctx->remote_addr.sin_family = AF_INET;

    if(inet_pton(AF_INET, addr, &ctx->remote_addr.sin_addr) != 1) {
        return -1;
    }

    HANDLE(ctx->sock = socket(AF_INET, SOCK_RAW | SOCK_NONBLOCK, IPPROTO_ICMP));

    ctx->last_ttl = 0;
    ctx->last_seq = 0;
    ctx->id = getpid();

    return 0;
}

int wait_for_next_hop(const struct traceroute_ctx* ctx, const struct waiting_for* waiting, struct reply* reply) {
    struct sockaddr_in sender;
    socklen_t sender_len = sizeof(sender);
    char buffer[2*IP_MAXPACKET];

    reply_clear(reply);

    int packets_left = PACKETS_PER_TRY;
    int time_left = ICMP_TIMEOUT;
    while(packets_left > 0) {
        int bytes_received = recvfrom_timeout(ctx->sock, buffer, IP_MAXPACKET, 0, (struct sockaddr*)&sender, &sender_len, &time_left);

        if(bytes_received == 0) {
            break; // timeout => no more packets
        }

        struct ip* ip_packet = (struct ip*) &buffer;

        for(int i = 0; i < PACKETS_PER_TRY; i++) {
            struct icmp* echo_reply = get_echo_reply_packet(ctx, waiting->seqs[i], ip_packet);
            if(echo_reply != NULL) {
                reply->is_echo_reply[i] = 1;
            } else {
                struct icmp* icmp_packet = get_ttl_packet(ctx, waiting->seqs[i], ip_packet);
                if(reply->durations[i] != -1 || icmp_packet == NULL) {
                    continue;
                }
            }

            reply->durations[i] = get_micros_elapsed(&(waiting->send_times[i]));
            reply->reply_addr[i] = sender.sin_addr;
            packets_left--;
            break;
        }
    }

    return 0;
}

int send_echo_requests(struct traceroute_ctx* ctx, struct waiting_for* result) {
    int ttl = ++ctx->last_ttl;

    for(int i = 0; i < PACKETS_PER_TRY; i++) {
        result->seqs[i] = ++ctx->last_seq;

        struct icmp packet;
        packet.icmp_type = ICMP_ECHO;
        packet.icmp_code = 0;
        packet.icmp_id = htons(ctx->id);
        packet.icmp_seq = htons(result->seqs[i]);
        packet.icmp_cksum = 0;
        packet.icmp_cksum = in_cksum((unsigned short*)&packet, 8, 0);

        HANDLE(setsockopt(ctx->sock, IPPROTO_IP, IP_TTL, &ttl, sizeof(int)));
        sendto_timeout(ctx->sock, &packet, ICMP_HEADER_LEN, 0, (struct sockaddr*)&(ctx->remote_addr), sizeof(ctx->remote_addr), ICMP_TIMEOUT);

        clock_gettime(CLOCK_MONOTONIC, &(result->send_times[i]));
    }

    return 0;
}

unsigned short in_cksum(const unsigned short *addr, register int len, unsigned short csum) {
    register int nleft = len;
    const unsigned short *w = addr;
    register unsigned short answer;
    register int sum = csum;

    /*   
     *  Our algorithm is simple, using a 32 bit accumulator (sum),
     *  we add sequential 16 bit words to it, and at the end, fold
     *  back all the carry bits from the top 16 bits into the lower
     *  16 bits.
     */
    while (nleft > 1)  {
        sum += *w++;
        nleft -= 2;
    }

    /* mop up an odd byte, if necessary */
    if (nleft == 1)
        sum += htons(*(unsigned char *)w << 8);

    /*
     * add back carry outs from top 16 bits to low 16 bits
     */
    sum = (sum >> 16) + (sum & 0xffff); /* add hi 16 to low 16 */
    sum += (sum >> 16);         /* add carry */
    answer = ~sum;              /* truncate to 16 bits */
    return (answer);
}

