// Łukasz Dąbek 247929
#include "common.h"

int get_micros_elapsed(const struct timespec* start) {
    struct timespec stop;

    clock_gettime(CLOCK_MONOTONIC, &stop);
    return (stop.tv_sec - start->tv_sec) * 1000000 + (stop.tv_nsec - start->tv_nsec) / 1000;
}

int micro_timespec_diff(const struct timespec* t1, const struct timespec* t2) {
    return (t1->tv_sec - t2->tv_sec) * 1000000 + (t1->tv_nsec - t2->tv_nsec) / 1000;
}

int timespec_cmp(const struct timespec* t1, const struct timespec* t2) {
    if(t1->tv_sec == t2->tv_sec)
        return t1->tv_nsec < t2->tv_nsec;
    return t1->tv_sec < t2->tv_sec;
}

