// Łukasz Dąbek 247929
#ifndef __SOCKETS_H__
#define __SOCKETS_H__

#define _POSIX_C_SOURCE 199309L
#define _BSD_SOURCE
#include <sys/types.h>
#include <netinet/ip.h>

int recvfrom_timeout(int sock, void* buf, size_t len, int flags, struct sockaddr* addr, socklen_t* addrlen, int* micros);
void sendto_timeout(int sock, const void* buf, size_t len, int flags, const struct sockaddr* addr, socklen_t addrlen, int micros);

#endif
