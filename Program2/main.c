// Łukasz Dąbek 247929
#define _POSIX_C_SOURCE 199309L
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <error.h>
#include <string.h>
#include <strings.h>
#include <arpa/inet.h>

#include "common.h"
#include "sockets.h"

#define SLICE_SIZE 1000
#define MAX_SLICES 1024
#define WAITING_REQUESTS 100
#define REQUESTS_PER_SLICE 1

#define NEW 0
#define SENT 1
#define RECEIVED 2

#define HOST "156.17.4.30"
#define TIMEOUT 10000 // micros

#define SLICE2START(s) ((s) * SLICE_SIZE)
#define START2SLICE(s) ((s) / SLICE_SIZE)

struct {
    int state;
    struct timespec req_time;
} slices[MAX_SLICES];

int all_slices, sent_slices, received_slices;

char in_buffer[2*SLICE_SIZE];
char out_buffer[SLICE_SIZE];
char file_buffer[SLICE_SIZE * MAX_SLICES];

int sock;
socklen_t address_len;
struct sockaddr_in address;

void prepare_socket(int port);
int find_unsent_slice();
int most_timeouted_slice();
int slices_cnt(int length);
int is_packet_valid(struct sockaddr_in* addr);
void read_packet_data();
void prepare_slice_request(int slice);
void send_slice_request(int slice);
void send_slice_requests(int slice);
void write_output_file(char* path, int length);

int main(int argc, char** argv) {
    if(argc < 4) {
        fprintf(stderr, "usage: %s [port] [file] [bytes]\n", argv[0]);
        return 1;
    }

    int port = atoi(argv[1]);
    char* output_path = argv[2];
    int length = atoi(argv[3]);
    all_slices = slices_cnt(length);

    prepare_socket(port);

    // Send initial requests.
    for(int i = 0; i < all_slices && i < WAITING_REQUESTS; i++) {
        send_slice_requests(i);
    }

    struct timespec curtime;
    while(received_slices < all_slices) {
        clock_gettime(CLOCK_MONOTONIC, &curtime);

        // Send new requests for timeouted slices.
        int first_slice, micros_left;
        do {
            first_slice = most_timeouted_slice();

            micros_left = TIMEOUT + micro_timespec_diff(&slices[first_slice].req_time, &curtime);
            if(micros_left <= 0) {
                send_slice_requests(first_slice);
            }
        } while(micros_left <= 0);

        struct sockaddr_in srcaddr;
        socklen_t srcaddr_len;

        int n;
        do {
            n = recvfrom_timeout(sock, in_buffer, sizeof(in_buffer), 0, (struct sockaddr*)&srcaddr, &srcaddr_len, &micros_left);
            if(n != 0 && is_packet_valid(&srcaddr)) {
                read_packet_data();
                printf("%.2lf%%\n", 100.0 * (double)received_slices / all_slices);

                // If we have any untouched slices send request for one of them.
                if(received_slices + sent_slices != all_slices) {
                    send_slice_requests(find_unsent_slice());
                }

                break;
            }
        } while(n > 0);
    }

    write_output_file(output_path, length);

    return 0;
}

void prepare_socket(int port) {
    HANDLE(sock = socket(AF_INET, SOCK_DGRAM, 0));

    bzero(&address, sizeof(address));
    address_len = sizeof(struct sockaddr_in);

    address.sin_family = AF_INET;
    address.sin_port = htons(port);
    if(inet_pton(AF_INET, HOST, &address.sin_addr) != 1) {
        exit(1);
    }
}

void write_output_file(char* path, int length) {
    FILE* f = fopen(path, "w");
    if(!f) {
        fprintf(stderr, "Cannot open output file\n");
        exit(1);
    }

    if(fwrite(file_buffer, 1, length, f) != (unsigned)length) {
        fprintf(stderr, "Cannot write to output file\n");
        exit(1);
    }

    fclose(f);
}

int is_packet_valid(struct sockaddr_in* addr) {
    if(addr->sin_port != address.sin_port || addr->sin_addr.s_addr != address.sin_addr.s_addr) {
        return 0;
    }

    int start, len;
    if(sscanf(in_buffer, "DATA %d %d", &start, &len) != 2) {
        return 0;
    }

    if(len != SLICE_SIZE) {
        return 0;
    }

    int slice = START2SLICE(start);
    if(slices[slice].state != SENT) {
        return 0;
    }

    return 1;
}

void read_packet_data() {
    int start, len;
    sscanf(in_buffer, "DATA %d %d", &start, &len);

    int slice = START2SLICE(start);
    char* data = strchr(in_buffer, '\n') + 1;

    for(int i = 0; i < len; i++) {
        file_buffer[start + i] = data[i];
    }

    slices[slice].state = RECEIVED;
    received_slices++;
    sent_slices--;
}

void prepare_slice_request(int slice) {
    sprintf(out_buffer, "GET %d %d\n", SLICE2START(slice), SLICE_SIZE);

    if(slices[slice].state == NEW) {
        slices[slice].state = SENT;
        sent_slices++;
    }

    clock_gettime(CLOCK_MONOTONIC, &slices[slice].req_time);
}

void send_slice_request(int slice) {
    prepare_slice_request(slice);
    sendto_timeout(sock, out_buffer, strlen(out_buffer), 0, (struct sockaddr*)&address, address_len, TIMEOUT);
}

void send_slice_requests(int slice) {
    for(int i = 0; i < REQUESTS_PER_SLICE; i++)
        send_slice_request(slice);
}

int slices_cnt(int length) {
    int cnt = length / SLICE_SIZE;

    if(length != cnt * SLICE_SIZE)
        cnt++;

    return cnt;
}

int most_timeouted_slice() {
    int first = 0;
    for(int i = 0; i < all_slices; i++) {
        if(slices[i].state == SENT) {
            first = i;
            break;
        }
    }

    for(int i = first + 1; i < all_slices; i++) {
        if(slices[i].state != SENT)
            continue;

        if(timespec_cmp(&slices[i].req_time, &slices[first].req_time)) {
            first = i;
        }
    }

    return first;
}

int find_unsent_slice() {
    for(int i = 0; i < all_slices; i++) {
        if(slices[i].state == NEW)
            return i;
    }

    printf("BUG - no unsent slices!\n");
    return -1;
}

