// Łukasz Dąbek 247929
#define _POSIX_C_SOURCE 199309L
#define _BSD_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <error.h>
#include <string.h>
#include <ctype.h>
#include <strings.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <limits.h>
#include <signal.h>

#include "common.h"
#include "sockets.h"

#define LISTEN_BACKLOG 50

struct req {
    char* path;
    char* host;
    int closeconn;
    int invalid;
};

struct res {
    int code;
    const char* type;
    int len;
    char* body;
};

int create_server(int port);
struct req read_request(int client);
struct res create_response(struct req* request);
char* parse_request_header(char* buffer);
char* find_header(const char* name, char* buffer);
void send_response(int client, struct res* response);
char* read_file(char* path, int* length_ret);
const char* status_code_text(int code);
const char* detect_content_type(char* path);
void free_req(struct req* req);
void free_res(struct res* res);

int main(int argc, char** argv) {
    if(argc < 3) {
        fprintf(stderr, "usage: %s [port] [dir]\n", argv[0]);
        return 1;
    }

    int port = atoi(argv[1]);
    char* dir = argv[2];

    HANDLE(chdir(dir));
    signal(SIGPIPE, SIG_IGN);

    int server_sock = create_server(port);

    while(1) {
        int client;
        HANDLE(client = accept(server_sock, NULL, NULL));

        struct req request = read_request(client);
        struct res response = create_response(&request);

        printf("Got request: host %s, path %s, closeconn: %d, invalid: %d\n", request.host, request.path, request.closeconn, request.invalid);
        send_response(client, &response);
        free_req(&request);
        free_res(&response);

        if(request.invalid)
            close(client);
    }

    close(server_sock);

    return 0;
}

const char* status_code_text(int code) {
    switch(code) {
        case 200: return "OK";
        case 301: return "Moved permanently";
        case 403: return "Forbidden";
        case 404: return "Not found";
        case 501: return "Not implemented";
        default: return "Unknown";
    }

    return NULL; // unreachable
}

const char* detect_content_type(char* path) {
    char* last_dot = strrchr(path, '.');
    if(!last_dot)
        return "application/octet-stream";

    last_dot++;
    if(strcasecmp(last_dot, "jpg") == 0 || strcasecmp(last_dot, "jpeg") == 0)
        return "image/jpeg";

    if(strcasecmp(last_dot, "html") == 0)
        return "text/html";

    if(strcasecmp(last_dot, "txt") == 0)
        return "text/plain";

    if(strcasecmp(last_dot, "css") == 0)
        return "text/css";

    if(strcasecmp(last_dot, "png") == 0)
        return "image/png";

    if(strcasecmp(last_dot, "pdf") == 0)
        return "application/pdf";

    return "application/octet-stream";
}

char* read_file(char* path, int* length_ret) {
    char* buffer = NULL;
    int length;
    FILE* f = fopen(path, "rb");

    if(!f) {
        return NULL;
    }

    fseek(f, 0, SEEK_END);
    length = ftell(f);
    fseek(f, 0, SEEK_SET);
    buffer = malloc(length);
    fread(buffer, 1, length, f);
    fclose(f);

    *length_ret = length;
    return buffer;
}

char* parse_request_header(char* buffer) {
    char* endline_pos = strstr(buffer, "\r\n");
    if(!endline_pos)
        return NULL;

    if(strncmp(buffer, "GET", 3) != 0) {
        printf("NO GET\n");
        return NULL;
    }

    char* ptr = &buffer[3];
    while(ptr < endline_pos && isspace(*ptr))
        ptr++;

    char* pathend = ptr;
    while(pathend < endline_pos && !isspace(*pathend))
        pathend++;

    if(ptr == pathend) {
        printf("NO PATH END\n");
        return NULL;
    }

    int path_len = pathend - ptr;
    char* path = malloc(path_len + 1);

    strncpy(path, ptr, path_len);
    path[path_len] = 0;

    return path;
}

char* find_header(const char* name, char* buffer) {
    char* endline_pos = strstr(buffer, "\r\n");
    if(!endline_pos)
        return NULL;

    char* header_pos = strstr(endline_pos, name);
    if(!header_pos)
        return NULL;

    char* colon_pos = strchr(header_pos, ':');
    if(!colon_pos)
        return NULL;

    char* value_pos = colon_pos + 1;
    while(*value_pos && isspace(*value_pos))
        value_pos++;

    char* value_end = strstr(value_pos, "\r\n");
    if(!value_end)
        return NULL;

    int len = value_end - value_pos;
    char* value = malloc(len + 1);

    strncpy(value, value_pos, len);
    value[len] = 0;

    return value;
}

struct req read_request(int client) {
    struct req request;
    char buffer[8096];

    int timeout = 1000000; // 1 sec
    int bytes_left = sizeof(buffer) - 1;
    int doublenl_found = 0;
    char* bufptr = (char*)(&buffer[0]);

    while(!doublenl_found && bytes_left > 0 && timeout > 0) {
        int bytes_read = recvfrom_timeout(client, bufptr, bytes_left, 0, NULL, NULL, &timeout);
        if(bytes_read <= 0)
            break;

        bytes_left -= bytes_read;
        bufptr += bytes_read;

        bufptr[0] = 0;

        if(strstr(buffer, "\r\n\r\n") != NULL) {
            doublenl_found = 1;
        }
    }

    printf("DATA: %s\n", buffer);

    if(timeout <= 0 || !doublenl_found) {
        request.host = request.path = NULL;
        request.invalid = 1;

        printf("Timeout: %d, doublenl: %d\n", timeout, doublenl_found);

        return request;
    }

    request.path = parse_request_header(&buffer[0]);
    request.host = find_header("Host", &buffer[0]);
    char* connection = find_header("Connection", &buffer[0]);

    if(!request.path || !request.host) {
        request.invalid = 1;
    } else {
        request.closeconn = 0;
        if(connection && strncmp(connection, "close", 5) == 0)
            request.closeconn = 1;

        request.invalid = 0;
    }

    if(request.host) {
        char* colon = strchr(request.host, ':');
        if(colon)
            *colon = 0;
    }

    return request;
}

struct res create_response(struct req* request) {
    char path[PATH_MAX];
    struct res response;

    response.type = "text/html";
    if(request->invalid) {
        response.code = 501;
        response.body = strdup("Not implemented");
        response.len = strlen(response.body);

        return response;
    }

    // TODO: security
    snprintf(path, PATH_MAX, "%s%s", request->host, request->path);

    int length;
    char* body = read_file(path, &length);
    if(!body) {
        response.body = strdup("NIE DA SIE");
        response.code = 404;
        response.len = strlen(response.body);
    } else {
        response.body = body;
        response.code = 200;
        response.type = detect_content_type(request->path);
        response.len = length;
    }

    return response;
}

void send_response(int client, struct res* response) {
    char buffer[8096];

    snprintf(buffer, sizeof(buffer),
             "HTTP/1.1 %d %s\r\nContent-Type: %s\r\nContent-Length: %d\r\n\r\n",
             response->code,
             status_code_text(response->code),
             response->type,
             response->len);

    sendto_timeout(client, buffer, strlen(buffer), 0, NULL, 0, 1000000);
    sendto_timeout(client, response->body, response->len, 0, NULL, 0, 1000000);
}

int create_server(int port) {
    int sock;
    struct sockaddr_in server_addr;

    HANDLE(sock = socket(AF_INET, SOCK_STREAM, 0));

    memset(&server_addr, 0, sizeof(struct sockaddr_in));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);
    inet_aton("0.0.0.0", (struct in_addr*)&server_addr.sin_addr.s_addr);

    HANDLE(bind(sock, (struct sockaddr*) &server_addr, (socklen_t) sizeof(struct sockaddr_in)));
    HANDLE(listen(sock, LISTEN_BACKLOG));

    return sock;
}

void free_req(struct req* req) {
    free(req->path);
    free(req->host);
}

void free_res(struct res* res) {
    free(res->body);
}

